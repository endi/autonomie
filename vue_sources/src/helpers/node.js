/**
 * Node related helpers (strings, icons, routes)NODENODE
 */
export const NODE_TYPE_ROUTES = {
    estimation: 'estimations',
    invoice: 'invoices',
    cancelinvoice: 'cancelinvoices',
    internalestimation: 'estimations',
    internalinvoice: 'invoices',
    internalcancelinvoice: 'cancelinvoices',
    business: 'businesses',
    project: 'projects',
  }
  export const NODE_ICONS = {
    estimation: 'file-list',
    invoice: 'file-invoice-euro',
    internalestimation: 'file-list',
    internalinvoice: 'file-invoice-euro',
    cancelinvoice: 'file-invoice-euro',
    internalcancelinvoice: 'file-invoice-euro',
    business: 'list-alt',
    project: 'folder',
  }
  export const NODE_STATUS_ICONS = {
    'draft': 'pen',
    'valid': 'check-circle',
    'invalid': 'times-circle',
    'wait': 'clock',
  }
  export const NODE_TYPE_LABELS = {
    project: 'Dossier',
    business: 'Affaire',
    estimation: 'Devis',
    invoice: 'Facture',
    cancelinvoice: 'Avoir',
    internalestimation: 'Devis interne',
    internalinvoice: 'Facture interne',
    internalcancelinvoice: 'Avoir interne',
  }
  export const NODE_TYPE_SIMPLE = {
    'internalestimation': 'estimation',
    'estimation': 'estimation',
    'invoice': 'invoice',
    internalinvoice: 'invoice',
    cancelinvoice: 'cancelinvoice',
    internalcancelinvoice: 'cancelinvoice',
  }
  export const NODE_STATUS_LABELS = {
    'estimation': {
        'draft': 'Devis en brouillon',
        'valid': 'Devis validé',
        'invalid': 'Devis invalidé',
        'wait': 'Devis en attente de validation',
    },
    'invoice':{
        'draft': 'Facture en brouillon',
        'valid': 'Facture validée',
        'invalid': 'Facture invalidée',
        'wait': 'Facture en attente de validation',
        'paid': 'Facture payée partiellement',
        'resulted': 'Facture soldée',
    },
    cancelinvoice: {
        'draft': 'Avoir en brouillon',
        'valid': 'Avoir validé',
        'invalid': 'Avoir invalidé',
        'wait': 'Avoir en attente de validation',
    },    
  }
  
  export const NODE_TYPE_SENTENCE_LABELS = {
    estimation: 'au Devis',
    invoice: 'à la Facture',
    cancelinvoice: "à l'Avoir",
    internalestimation: 'au Devis interne',
    internalinvoice: 'à la Facture interne',
    internalcancelinvoice: "à l'Avoir interne",
    project: 'au Dossier',
    business: "à l'Affaire",
  }
  
  
  export const getNodeStatusIcon = (node) => NODE_STATUS_ICONS[node.status]
  export const getNodeStatusString = (node) => {
    const simpleType = NODE_TYPE_SIMPLE[node.type_]
    return NODE_STATUS_LABELS[simpleType][node.status]
  }
  export const getTaskMainNumber = (node) => {
    if(node.type_.includes('invoice')) {
      return node.official_number
    }else if(node.type_.includes('estimation')) {
      return node.internal_number
    }else{
      return undefined
    }
  }
