API REST
=========

Urls
-----

.. list-table:: Mapping des urls rest d'une resource
    :widths: 20 20 20 20 20
    :header-rows: 1

    * - URL / Méthode
      - GET
      - POST
      - PUT
      - DELETE
    * - Collection Url
      - Renvoie la collection
      - Ajoute
      - X
      - X
    * - Item Url
      - Renvoie l'item
      - Modifie l'élément
      - Modifie l'élément
      - Supprime l'élément
