.. CAERP documentation master file, created by
   sphinx-quickstart on Fri Jan 25 10:44:18 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

CAERP
=========

Contents:

.. toctree::
    :maxdepth: 2

    components
    decoupage
    lexique
    vente/index.rst
    comptabilite/index.rst
    javascript/index.rst
    formulaire_javascript
    sqlalchemy_index
    views
    classes
    styleguide
    pyramid_traversal
    permissions
    celery
    service
    tests
    caerpsap
    caerpsolo
    notification/index.rst
    apidoc/caerp.rst
    apidoc/caerp_celery.rst
    apidoc/caerp_base.rst
    js_templates


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

