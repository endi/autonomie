Découpage d'enDi : modules optionnels, plugins, dépôts…
========================================================

.. warning:: On parle ici de notions propres à enDi, aussi un « module » n'est
   pas nécessairement un « module python ».

Les dépôts git
---------------

Le logiciel enDi est découpé entre plusieurs dépôts git qui sont regroupés
dans le `namespace caerp sur framagit`_.

La plupart du temps, pour le développement, seul le dépôt principal *caerp/caerp*
est nécessaire.

.. _`namespace caerp sur framagit`:

   https://framagit.org/caerp

Les modules optionnels
-----------------------

Les **modules optionnels** sont des parties d'enDi que l'on peut *désactiver*
pour une installation donnée. Par défaut tous les modules optionnels sont
activés.

Exemple : on peut désactiver le module formation si la CAE ne fait pas de
formation profesionnelle.

Ils se gèrent via la clef de configuration ``caerp.modules`` dans le fichier .ini.

Par exemple, pour désactiver tous les modules optionnels (liste vide):

.. code-block:: ini

   caerp.modules =

Ou bien par n'activer que l'accompagnement et les ateliers :


.. code-block:: ini

   caerp.modules =
              caerp.views.accompagnement
              caerp.views.workshops


.. note:: Il est possible de trouver la liste des modules optionnels dans le
          fichier ``caerp/__init__.py`` ; variable ``CAERP_OTHER_MODULES``.

Développement de modules optionnels
-----------------------------------

Outils :
~~~~~~~~

Il est parfois nécessaire de faire différement dans le code selon qu'un module
optionnel est activé ou non (ex: afficher tel ou tel lien) :

.. note::

   if request.has_module('caerp.views.workshops'):
       ...



.. _plugins:

Les plugins
-----------

Les plugins vont modifier le comportement d'enDi sur plusieurs aspects :

- modifier les menus
- remplacer des routes/vues
- charger des données supplémentaires d'initialisation (`caerp-admin syncdb`)
- ajouter des commandes (`caerp-admin <nouvelle-commande>`)
- ajouter des modèles
- implémenter des `services` (cf :doc:`service` )

.. note::
   Les plugins existants à ce jour sont :

   - caerp_payment⭐
   - caerp_edocsafe⭐
   - caerp_oidc⭐
   - caerp.plugins.solo
   - caerp.plugins.sap
   - caerp.plugins.sap_urssaf3p

   ⭐ : dans un dépôt git à part.


Par défaut aucun plugin n'est activé, pour activer un plugin, il faut définir
l'entrée suivante dans le fichier .ini (qui contient donc une liste) :

.. code-block:: ini

   caerp.includes =
           caerp.plugins.solo


Les procédures pour activer un plugin peuvent être plus complexes selon le
plugin, auquel cas elles sont décrites dans sa documentation propre.

Développement de plugins
-------------------------

Outils :
~~~~~~~~

Lorsqu'il est trop compliqué de faire autrement, il est parfois nécessaire
d'avoir dans le code principal un code conditionellement à la présence ou non d'un plugin.

.. note::

   if request.has_plugin('caerp_payment'):
       ...

Commandes caerp-admin :
~~~~~~~~~~~~~~~~~~~~~~

Les plugins peuvent ajouter assez simplement des sous-commandes de *caerp-admin* ; pour voir comment faire, regarder l'exemple de ce que fait le plugin sap_urssaf3p qui ajoute une commande ``caerp-admin check_urssaf3p``.


Bonnes pratiques :
~~~~~~~~~~~~~~~~~~


Concernant les modèles:

- Pour ajouter des champs : éviter de surcharger/redéfinir des modèles/tables (SQLAlchemy) existants à
  l'intérieur d'un plugin… : modifier directement le modèle de base.
- Éviter de créer des modèles liés (ForeignKey, héritage…) aux autres modèles
  d'enDi.
- Importer inconditionellement (que le plugin soit actif ou non) les modèles apportés par les plugins
  depuis `caerp/models/__init__.py`
- Identifier explicitement dans le code les champs/modèles ajoutés pour les
  besoins d'un plugin en particulier

Concernant les tests

- les tests sont lancés avec tous les plugins désactivés

.. note::

   Il n'y a pas actuellement de structure pour avoir des fichiers statiques
   (ex: JS/CSS) hors de l'application principale.
