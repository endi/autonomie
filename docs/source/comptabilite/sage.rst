Export des écritures au format Sage
===================================

Les écritures sont exportées par défaut au format Sage dans des formats CSV écrit dans des fichiers .txt.

La configuration par défaut est caerp/__init__.py.

Elle correspond à la configuration suivante dans le fichier .ini

.. code-block::

    caerp.services.treasury_invoice_producer=caerp.compute.sage.InvoiceExportProducer
    caerp.services.treasury_internalinvoice_producer=caerp.compute.sage.InternalInvoiceExportProducer
    caerp.services.treasury_invoice_writer=caerp.export.sage.SageInvoiceCsvWriter

    caerp.services.treasury_payment_producer=caerp.compute.sage.PaymentExportProducer
    caerp.services.treasury_internalpayment_producer=caerp.compute.sage.InternalPaymentExportProducer
    caerp.services.treasury_payment_writer=caerp.export.sage.SagePaymentCsvWriter

    caerp.services.treasury_expense_producer=caerp.compute.sage.ExpenseExportProducer
    caerp.services.treasury_expense_writer=caerp.export.sage.SageExpenseCsvWriter

    caerp.services.treasury_expense_payment_producer=caerp.compute.sage.ExpensePaymentExportProducer
    caerp.services.treasury_expense_payment_writer=caerp.export.sage.SageExpensePaymentCsvWriter

    caerp.services.treasury_supplier_invoice_producer=caerp.compute.sage.SupplierInvoiceExportProducer
    caerp.services.treasury_internalsupplier_invoice_producer=caerp.compute.sage.InternalSupplierInvoiceExportProducer
    caerp.services.treasury_supplier_invoice_writer=caerp.export.sage.SageSupplierInvoiceCsvWriter

    caerp.services.treasury_supplier_payment_producer=caerp.compute.sage.SupplierPaymentExportProducer
    caerp.services.treasury_supplier_payment_user_producer=caerp.compute.sage.SupplierUserPaymentExportProducer
    caerp.services.treasury_internalsupplier_payment_producer=caerp.compute.sage.InternalSupplierPaymentExportProducer
    caerp.services.treasury_supplier_payment_writer=caerp.export.sage.SageSupplierPaymentCsvWriter
