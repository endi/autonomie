import Mn from "backbone.marionette"
import InputWidget from "../../../../../widgets/InputWidget.js"
import SelectWidget from "../../../../../widgets/SelectWidget.js"
import TextAreaWidget from "../../../../../widgets/TextAreaWidget.js"
import FormBehavior from "../../../../../base/behaviors/FormBehavior.js"
import Radio from "backbone.radio"
var template = require("./templates/PostTTCLineFormView.mustache")

const PostTTCLineFormView = Mn.View.extend({
    tagName: "div",
    className: "modal_overflow",
    behaviors: [FormBehavior],
    template: template,
    regions: {
        label: ".label",
        amount: ".amount",
    },
    childViewTriggers: {
        change: "data:modified",
    },
    initialize() {
        const config = Radio.channel("config")
    },
    onRender: function () {
        let label
        this.showChildView(
            "label",
            new InputWidget({
                value: this.model.get("label"),
                title: "Libellé",
                field_name: "label",
                cid: this.model.cid,
                required: true,
            })
        )
        this.showChildView(
            "amount",
            new InputWidget({
                value: this.model.get("amount"),
                label: "Montant",
                field_name: "amount",
                required: true,
            })
        )
    },
    templateContext: function () {
        return {
            title: this.getOption("title"),
        }
    },
})
export default PostTTCLineFormView
