/**
 * Custom non marionette views used to render non hierarchical structures
 *
 * Here we render the html snippets for the different products/work/work_item
 * elements that are rendered in tbody s
 *
 */
import Radio from "backbone.radio"

const PRODUCT_VIEW_TEMPLATE = require("./templates/ProductRawView.mustache")
const WORK_VIEW_TEMPLATE = require("./templates/WorkRawView.mustache")
const WORK_ITEM_VIEW_TEMPLATE = require("./templates/WorkItemRawView.mustache")

/**
 *
 * @param {BaseModel} model
 * @returns template context variables used to order the model
 */
const _OrderContextVariable = (model) => {
    let min_order = model.collection.getMinOrder()
    let max_order = model.collection.getMaxOrder()
    let order = model.get("order")
    return {
        is_not_first: order != min_order,
        is_not_last: order != max_order,
    }
}

const _tva_labels = (model) => {
    const tva = model.tva_object()

    const result = {}
    if (tva) {
        result["tva_label"] = tva.label
    }
    const product = model.product_object()
    if (product) {
        result["product_label"] = product.label
    }
    return result
}
const _modeContext = (model, index) => {
    return {
        supplier_ht_mode: model.get("mode") === "supplier_ht",
    }
}
/**
 *
 * @returns template vars related to the current price study model
 */
const _priceStudyContext = () => {
    const model = Radio.channel("facade").request(
        "get:model",
        "display_options"
    )
    return {
        display_units: model.get("display_units"),
    }
}
/**
 *
 * @param {BaseProductModel} model
 * @param {integer} index
 * @returns Template variables related to the Product
 */
const _baseProductContext = (model, index) => {
    let ctx = _OrderContextVariable(model)
    ctx = Object.assign(ctx, model.attributes)
    const user_prefs = Radio.channel("user_preferences")
    for (const key of ["supplier_ht", "ht", "total_ht"]) {
        ctx[key] = user_prefs.request("formatAmount", model.get(key), true)
    }
    ctx = Object.assign(ctx, _tva_labels(model))
    ctx = Object.assign(ctx, _modeContext(model, index))
    ctx = Object.assign(ctx, _priceStudyContext())
    ctx["htmlIndex"] = index

    return ctx
}
/**
 *
 * @param {ProductCollection} collection
 * @returns The representation of the collection as a serie of tbodys
 */
export const getProductCollectionHtml = function (collection) {
    let result = ""
    collection.each((model, index) => {
        if (model.get("type_") == "price_study_work") {
            result += getWorkViewHtml(model, index)
        } else {
            result += getProductViewHtml(model, index)
        }
    })
    return result
}
/**
 *
 * @param {ProductModel} model
 * @param {number} index
 * @returns The html representation of this product model
 */
export const getProductViewHtml = function (model, index) {
    const ctx = _baseProductContext(model, index)
    return PRODUCT_VIEW_TEMPLATE(ctx)
}
/**
 *
 * @param {WorkModel} model
 * @param {number} index
 * @returns An html representation of the model and its items
 */
export const getWorkViewHtml = function (model, index) {
    let itemsRawHtml = ""
    model.items.each((item, index) => {
        itemsRawHtml += getWorkItemViewHtml(item, index, model)
    })
    let ctx = _baseProductContext(model, index)
    ctx["workitemsHtml"] = itemsRawHtml

    return WORK_VIEW_TEMPLATE(ctx)
}
/**
 *
 * @param {WorkItemModel} model
 * @param {number} index
 * @returns A html representation of the WorkItemModel
 */
export const getWorkItemViewHtml = function (model, index, work) {
    let ctx = Object.assign({}, model.attributes)
    ctx["htmlIndex"] = index
    const user_prefs = Radio.channel("user_preferences")
    for (const key of ["ht", "supplier_ht", "work_unit_ht", "total_ht"]) {
        ctx[key] = user_prefs.request("formatAmount", model.get(key), true)
    }
    ctx = Object.assign(ctx, _priceStudyContext())
    ctx = Object.assign(ctx, _OrderContextVariable(model))
    ctx = Object.assign(ctx, _modeContext(model, index))
    ctx["hidden"] = !work.get("display_details")
    if (model.get("quantity_inherited")) {
        ctx["quantity_label"] =
            "" + model.get("work_unit_quantity") + " x " + work.get("quantity")
    } else {
        ctx["quantity_label"] = false
    }

    return WORK_ITEM_VIEW_TEMPLATE(ctx)
}
