8
/*
 * Module name : PriceStudyView
 */
import Mn from "backbone.marionette"
import Radio from "backbone.radio"

import FormBehavior from "base/behaviors/FormBehavior.js"
import InputWidget from "widgets/InputWidget.js"
import CheckboxWidget from "widgets/CheckboxWidget"

const template = require("./templates/PriceStudyView.mustache")
/**
 * View presenting main Price Study parameters configuration
 * - general overhead
 * - reset margin rate to global value
 */
const PriceStudyView = Mn.View.extend({
    className: "form-section",
    behaviors: [FormBehavior],
    template: template,
    regions: {
        general_overhead: ".field-general_overhead",
        margin_rate: ".field-margin_rate",
        mask_hours: ".field-mask_hours",
    },
    // Bubble up child view events
    childViewTriggers: {
        change: "data:modified",
        finish: "data:persist",
    },
    childViewEvents: {
        "margin_rate:reset": "onMarginRateReset",
    },
    initialize() {
        this.app = Radio.channel("priceStudyApp")
        this.section = this.getOption("section") || {}
    },
    showMaskHours() {
        const view = new CheckboxWidget({
            field_name: "mask_hours",
            title: "",
            inline_label: "Masquer les unités de main d’œuvre dans le PDF",
            description:
                "Pour les prestations de main d’œuvre, l’unité affichée dans le PDF final sera « forfait ». ",
            true_val: true,
            false_val: false,
            value: this.model.get("mask_hours"),
        })
        this.showChildView("mask_hours", view)
    },
    showGeneralOverhead() {
        this.showChildView(
            "general_overhead",
            new InputWidget({
                field_name: "general_overhead",
                title: "Coefficient de frais généraux",
                required: true,
                value: this.model.get("general_overhead"),
            })
        )
    },
    showMarginRate() {
        const label =
            "Remettre tous les coefficients de marge à la valeur par défaut (" +
            this.model.get("margin_rate") +
            ")"
        this.showChildView(
            "margin_rate",
            new CheckboxWidget({
                title: "",
                inline_label: label,
                value: 0,
                finishEventName: "margin_rate:reset",
            })
        )
    },
    onMarginRateReset() {
        this.app.trigger("reset:margin_rate")
    },
    onRender() {
        this.showMaskHours()
        if (this.section.general_overhead.edit) {
            this.showGeneralOverhead()
        }
        if (this.section.margin_rate.edit && this.model.get("margin_rate")) {
            this.showMarginRate()
        }
    },
    onSuccessSync() {
        this.app.trigger("product:changed")
    },
})
export default PriceStudyView
