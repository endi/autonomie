/*
 * Module name : DiscountView
 */
import Mn from "backbone.marionette"
import Radio from "backbone.radio"

const template = require("./templates/DiscountView.mustache")

const DiscountView = Mn.View.extend({
    template: template,
    tagName: "tr",
    events: {
        "click button.delete": "onDeleteClicked",
        "click button.edit": "onEditClicked",
        "click button.up": "onUpClicked",
        "click button.down": "onDownClicked",
    },
    modelEvents: {
        sync: "render",
    },
    initialize() {
        this.user_prefs = Radio.channel("user_preferences")
    },
    templateContext() {
        let min_order = this.model.collection.getMinOrder()
        let max_order = this.model.collection.getMaxOrder()
        let order = this.model.get("order")
        return {
            tva_label: this.model.tva_label(),
            is_percentage: this.model.is_percentage(),
            total_ht_label: this.user_prefs.request(
                "formatAmount",
                this.model.get("total_ht"),
                false
            ),
            is_not_first: order != min_order,
            is_not_last: order != max_order,
        }
    },
    // On remonte les events jusqu'au DiscountComponent
    onDeleteClicked() {
        this.triggerMethod("delete", this.model)
    },
    onEditClicked() {
        this.triggerMethod("edit", this.model)
    },
    onUpClicked() {
        this.triggerMethod("up", this.model)
    },
    onDownClicked() {
        this.triggerMethod("down", this.model)
    },
})
export default DiscountView
