import Bb from "backbone"
import DiscountModel from "./DiscountModel.js"
import Radio from "backbone.radio"
import OrderableCollection from "base/models/OrderableCollection.js"

const DiscountCollection = OrderableCollection.extend({
    amount_related_props: ["amount", "percentage", "tva_id"],
    model: DiscountModel,
    initialize: function (options) {
        this.on("saved", this.channelCall)
        this.on("remove", this.onDeleteCall)
    },
    onDeleteCall() {
        var channel = Radio.channel("priceStudyFacade")
        channel.trigger("changed:discount")
    },
    channelCall: function (key_or_attributes) {
        var channel = Radio.channel("priceStudyFacade")
        let changed_keys
        if (typeof key_or_attributes == "object") {
            changed_keys = [key_or_attributes]
        } else {
            changed_keys = _.keys(key_or_attributes)
        }

        let fire = Boolean(
            _.intersection(this.amount_related_props, key_or_attributes)
        )
        if (fire) {
            channel.trigger("changed:discount")
        }
    },
})
export default DiscountCollection
