import Mn from "backbone.marionette"
import Radio from "backbone.radio"

const template = require("./templates/CombinationView.mustache")

const CombinationView = Mn.View.extend({
    template: template,
    regions: {},
    ui: { button: "input[type=radio]" },
    events: { "click @ui.button": "onClick" },
    modelEvents: { saved: "render" },
    childViewEvents: {
        "add:click": "onAddClick",
    },
    initialize() {
        this.config = Radio.channel("config")
        this.facade = Radio.channel("facade")
    },
    onAddClick() {
        console.log("CombinationView.onAddClick")
        this.trigger("add:click", this.model)
    },
    onClick(event) {
        const value = event.target.value
        this.model.set({ type: value })
        this.model.save({ patch: true, wait: true })
    },
    templateContext() {
        return {
            is_and_clause: this.model.get("type") == "and",
            is_or_clause: this.model.get("type") == "or",
        }
    },
})
export default CombinationView
