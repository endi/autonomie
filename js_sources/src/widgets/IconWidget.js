/*
 * Easily design icons
 *
 * Find list of icons in the endi.svg file
 *
 * this.showChildView('icon-container', new IconWidget({icon: icon}));
 *

*/
/* global ENDI_STATIC_ICON_URL; */
import Mn from "backbone.marionette"
import { viewToString } from "../tools"

const IconWidget = Mn.View.extend({
    // NB : we need to use a span since using the tagName : 'svg' doesn't work (icon not loaded)
    tagName: "span",
    template: require("./templates/IconWidget.mustache"),
    templateContext() {
        return { url: CAERP_STATIC_ICON_URL + "#" + this.getOption("icon") }
    },
})
export const iconHtmlString = (icon) => {
    return viewToString(new IconWidget({ icon: icon }))
}
export default IconWidget
