import Mn from "backbone.marionette"
import { getOpt } from "../tools.js"

const LabelRowWidget = Mn.View.extend({
    tagName: "tr",
    className: "row_recap",
    template: require("./templates/LabelRowWidget.mustache"),
    templateContext: function () {
        var values = this.getOption("values")
        var label = getOpt(this, "label", "")

        if (!Array.isArray(values)) {
            values = [{ label: label, value: values }]
        }
        return {
            values: values,
            colspan: getOpt(this, "colspan", 1),
        }
    },
})
export default LabelRowWidget
