import Mn from "backbone.marionette"
import ModalBehavior from "base/behaviors/ModalBehavior.js"

import SelectableExpenseCollectionView from "./SelectableExpenseCollectionView.js"

const ExpenseFormPopupView = Mn.View.extend({
    behaviors: [ModalBehavior],
    ui: {
        modalbody: ".modal_content_layout",
        submit_btn: "input[type=submit]",
        cancel_btn: "input[type=reset]",
        form: "form",
    },
    regions: {
        lines: { el: "tbody", replaceElement: true },
    },
    events: {
        "submit @ui.form": "onSubmit",
    },
    showExpenseLineSelect() {
        let view = new SelectableExpenseCollectionView({
            collection: this.linesCollection,
            targetFile: this.model,
        })
        this.showChildView("lines", view)
    },

    template: require("./templates/FileLinkPopupView.mustache"),
    id: "expense-file-link-form-popup-modal",
    initialize() {
        this.model = this.getOption("model")
        this.linesCollection = this.getOption("linesCollection")
    },
    onSubmit(event) {
        event.preventDefault()
        this.triggerMethod("modal:close")
    },
    templateContext() {
        return {
            file_label: this.model.get("label"),
        }
    },
    onRender: function () {
        this.showExpenseLineSelect()
    },
})
export default ExpenseFormPopupView
