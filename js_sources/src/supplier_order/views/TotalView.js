import Mn from "backbone.marionette"
import Radio from "backbone.radio"
import { formatPrice, formatAmount } from "../../math.js"

const TotalView = Mn.View.extend({
    tagName: "div",
    template: require("./templates/TotalView.mustache"),
    modelEvents: {
        "change:ttc": "render",
        "change:ht": "render",
        "change:tva": "render",
        "change:ttc_cae": "render",
        "change:ttc_worker": "render",
    },
    templateContext() {
        return {
            ht: formatAmount(this.model.get("ht")),
            tva: formatAmount(this.model.get("tva")),
            ttc: formatAmount(this.model.get("ttc")),
            ttc_cae: formatAmount(this.model.get("ttc_cae")),
            ttc_worker: formatAmount(this.model.get("ttc_worker")),
        }
    },
})
export default TotalView
