import _ from "underscore"
import Mn from "backbone.marionette"

import ImageViewerView from "../views/ImageViewerView"
import PDFViewerView from "../views/PDFViewerView"
import ConfigBus from "../../base/components/ConfigBus"

/**
 * Specialized config bus responder for Expense Types
 *
 * Answers side-to-side with ConfigBus on same 'config' radio channel.
 */
const PreviewServiceClass = Mn.Object.extend({
    channelName: "config",
    radioRequests: {
        "is:previewable": "isPreviewable",
        "get:viewer:class": "getViewerClass",
    },
    typesMap: {
        "image/jpeg": ImageViewerView,
        "image/svg+xml": ImageViewerView,
        "image/gif": ImageViewerView,
        "image/bitmap": ImageViewerView,
        "image/png": ImageViewerView,
        "image/svg": ImageViewerView,
        "image/webp": ImageViewerView,
        "image/apng": ImageViewerView,
        "image/tiff": ImageViewerView,
        "application/pdf": PDFViewerView,
    },
    setup(form_config_url) {
        console.log("PreviewService.setup")
    },
    start() {
        console.log("PreviewService.start")
        let result = $.Deferred()
        result.resolve(ConfigBus.form_config, null, null)
        return result
    },
    /**
     * Tells wether or not the file is previewable in the browser
     *
     * @param {NodeFileModel} nodeFile
     * @return {boolean}
     */
    isPreviewable(nodeFile) {
        return this.typesMap.hasOwnProperty(nodeFile.get("mimetype"))
    },
    /**
     * Tells wether or not the file is previewable in the browser
     *
     * @param {NodeFileModel} nodeFile
     * @return {BaseDocumentViewer} (non-instantiated)
     */
    getViewerClass(nodeFile) {
        return this.typesMap[nodeFile.get("mimetype")]
    },
})

const PreviewService = new PreviewServiceClass()
export default PreviewService
