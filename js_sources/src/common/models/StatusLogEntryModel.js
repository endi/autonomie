import Bb from "backbone"
import Radio from "backbone.radio"
import BaseModel from "base/models/BaseModel"

const StatusLogEntryModel = BaseModel.extend({
    props: [
        "id",
        "datetime",
        "status",
        "label",
        "comment",
        "icon",
        "css_class",
        "user",
        "can_edit",
        "visibility",
        "pinned",
        "notify",
        "notification_recipients",
    ],
    defaults: function () {
        const config = Radio.channel("config")
        let recipients = config.request(
            "get:options",
            "notification_recipients"
        )
        if (!recipients) {
            recipients = []
        }
        return {
            label: "",
            comment: "",
            pinned: false,
            notify: recipients.length > 0,
            notification_recipients: recipients.map(function (recipient) {
                return recipient.id
            }),
        }
    },
    cleanProps(attributes) {
        // Turn this method into no-op to avoid discarding null-attrs
        // Discarding null-attrs as cleanProps does prevents null-valued
        // attributed to be synced correctly from server.
        return attributes
    },
})
export default StatusLogEntryModel
