/*
 * Module name : ProductEmptyView
 */
import Mn from "backbone.marionette"
import Radio from "backbone.radio"

const template = require("./templates/ProductEmptyView.mustache")

const ProductEmptyView = Mn.View.extend({
    tagName: "tr",
    template: template,
    regions: {},
    ui: {},
    // Listen to the current's view events
    events: {},
    // Listen to child view events
    childViewEvents: {},
    // Bubble up child view events
    childViewTriggers: {},
    initialize() {
        this.config = Radio.channel("config")
    },
    onRender() {},
    templateContext() {
        return {}
    },
})
export default ProductEmptyView
