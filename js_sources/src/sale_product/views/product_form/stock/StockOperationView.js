/*
 * Module name : StockOperationView
 */
import Mn from "backbone.marionette"
import Radio from "backbone.radio"
import ButtonModel from "../../../../base/models/ButtonModel.js"
import ButtonWidget from "../../../../widgets/ButtonWidget.js"
import { formatDate } from "../../../../date.js"

const template = require("./templates/StockOperationView.mustache")

const StockOperationView = Mn.View.extend({
    template: template,
    tagName: "tr",
    regions: {
        editButtonContainer: { el: ".col_actions .edit", replaceElement: true },
        delButtonContainer: {
            el: ".col_actions .delete",
            replaceElement: true,
        },
    },
    childViewEvents: {
        "action:clicked": "onActionClicked",
    },
    modelEvents: {
        change: "render",
    },
    initialize() {
        this.config = Radio.channel("config")
    },
    onRender() {
        let editModel = new ButtonModel({
            ariaLabel: "Modifier ce mouvement de stock",
            icon: "pen",
            showLabel: false,
            action: "edit",
        })
        let deleteModel = new ButtonModel({
            ariaLabel: "Supprimer ce mouvement de stock",
            css: "negative",
            icon: "trash-alt",
            showLabel: false,
            action: "delete",
        })
        this.showChildView(
            "editButtonContainer",
            new ButtonWidget({ model: editModel })
        )
        this.showChildView(
            "delButtonContainer",
            new ButtonWidget({ model: deleteModel })
        )
    },
    templateContext() {
        return {
            date: formatDate(this.model.get("date")),
            description: this.model.get("description"),
            stock_variation: this.model.get("stock_variation"),
        }
    },
    onActionClicked(action) {
        if (action == "edit") {
            this.triggerMethod("model:edit", this.model, this)
        } else if (action == "delete") {
            this.triggerMethod("model:delete", this.model, this)
        }
    },
})
export default StockOperationView
