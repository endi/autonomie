import BaseQualiopiKnowledgeSaleProduct from "./BaseQualiopiKnowledgeSaleProduct"

const VAEModel = BaseQualiopiKnowledgeSaleProduct.extend({
    props: ["eligibility_process"],
})
VAEModel.prototype.props = VAEModel.prototype.props.concat(
    BaseQualiopiKnowledgeSaleProduct.prototype.props
)
export default VAEModel
