import ProductWorkModel from "./ProductWorkModel.js"

/** Abstract class to handle model fields of BaseQualiopiSaleProduct back-end model
 */
const BaseQualiopiSaleProduct = ProductWorkModel.extend({
    props: [
        "access_delay",
        "accessibility",
        "duration_hours",
        "content",
        "group_size",
        "presence_modality",
        "results",
        "teaching_method",
        "trainer",
    ],
    getIcons() {
        return "chalkboard-teacher"
    },
})
BaseQualiopiSaleProduct.prototype.props =
    BaseQualiopiSaleProduct.prototype.props.concat(
        ProductWorkModel.prototype.props
    )

export default BaseQualiopiSaleProduct
