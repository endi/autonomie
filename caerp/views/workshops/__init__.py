def includeme(config):
    config.include(".routes")
    config.include(".workshop")
    config.include(".lists")
    config.include(".export")
