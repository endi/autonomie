def includeme(config):
    config.include(".tasks")
    config.include(".expenses")
    config.include(".supplier_orders")
    config.include(".supplier_invoices")
