import datetime
from unittest.mock import Mock
import io

import pytest

from caerp.export.sage import SageCsvWriter
from caerp.models.config import Config


@pytest.fixture
def mk_test_csv_writer():
    class TestWriter(SageCsvWriter):
        headers = [
            {"name": "libelle", "label": "Libellé"},
            {"name": "date", "label": "Date"},
        ]

    def _mk_test_csv_writer(datas):
        request = Mock()
        request.config = Config
        w = TestWriter(request=request, context=None)
        w.set_datas(datas)
        return w

    return _mk_test_csv_writer


def test_sage_csv_writer(dbsession, mk_test_csv_writer):
    def _init_writer():
        return mk_test_csv_writer(
            [
                {"libelle": "123456789", "date": datetime.date(2022, 1, 1)},
            ]
        )

    # default (no conf)
    buf = _init_writer().render(io.StringIO())
    buf.readline()  # header
    assert buf.readline().strip() == '"123456789";"010122"'

    # zero
    Config.set("accounting_label_maxlength", "0")
    buf = _init_writer().render(io.StringIO())
    buf.readline()  # header
    assert buf.readline().strip() == '"123456789";"010122"'

    # lower than size
    Config.set("accounting_label_maxlength", "4")
    buf = _init_writer().render(io.StringIO())
    buf.readline()  # header
    assert buf.readline().strip() == '"1234";"010122"'

    # bigger than size
    Config.set("accounting_label_maxlength", "11")
    buf = _init_writer().render(io.StringIO())
    buf.readline()  # header
    assert buf.readline().strip() == '"123456789";"010122"'


def test_csv_writer_strip_newlines(dbsession, mk_test_csv_writer):
    buf = mk_test_csv_writer(
        [
            {"libelle": "1\n\r2", "date": datetime.date(2022, 1, 1)},
        ]
    ).render(io.StringIO())
    buf.readline()  # header
    assert buf.readline().strip() == '"1  2";"010122"'
