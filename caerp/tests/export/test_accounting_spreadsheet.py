import pytest


def test_spreadsheet_formula():
    from caerp.export.accounting_spreadsheet import SpreadsheetFormula

    assert SpreadsheetFormula("=A1") == "=A1"
    with pytest.raises(ValueError):
        SpreadsheetFormula("A1")


def test_cells_index(
    income_statement_measure_type_categories,
    mk_income_measure_type,
):
    from caerp.export.accounting_spreadsheet import CellsIndex

    produits, achats = income_statement_measure_type_categories

    mt1 = mk_income_measure_type(
        category=produits,
        label="mt1",
    )
    mt2 = mk_income_measure_type(
        category=produits,
        label="mt2",
        is_total=True,
        total_type="account_prefix",
    )
    mt3 = mk_income_measure_type(
        category=produits,
        label="mt3",
        is_total=True,
        total_type="complex_total",
    )

    index = CellsIndex()
    assert index.category_lookup("Produits") == []

    index.register(mt1)
    assert index.category_lookup("Produits") == [0]
    assert index.category_lookup("Achats") == []
    assert index.type_lookup("mt1") == 0

    index.register(mt2)
    assert index.category_lookup("Produits") == [0, 1]
    assert index.category_lookup("Achats") == []

    index.register(mt3)
    assert index.category_lookup("Produits") == [
        0,
        1,
    ], "This total type is supposedly ignored in category totals"

    assert index.category_lookup("Achats") == []


def test_spreadsheet_coordinates_mapper():
    from caerp.export.accounting_spreadsheet import SpreadsheetCoordinatesMapper

    mapper = SpreadsheetCoordinatesMapper()
    assert mapper.to_spreadsheet_coords(0, 0) == "A1"
    # Detects a range
    assert list(mapper.to_spreadsheet_ranges([((0, 1), (0, 3))])) == [("A2", "A4")]
    # Detects a range + a single cell ref
    assert list(mapper.to_spreadsheet_ranges([((0, 1), (0, 3)), ((0, 5), (0, 5))])) == [
        ("A2", "A4"),
        ("A6", "A6"),
    ]


def test_spreadsheet_coordinates_mapper_w_offset():
    from caerp.export.accounting_spreadsheet import SpreadsheetCoordinatesMapper

    mapper = SpreadsheetCoordinatesMapper(1, 10)
    assert mapper.to_spreadsheet_coords(0, 1) == "B12"
    # Detects a range
    assert list(mapper.to_spreadsheet_ranges([((0, 1), (0, 3))])) == [
        ("B12", "B14"),
    ]
    # Detects a range + a single cell ref
    assert list(mapper.to_spreadsheet_ranges([((0, 1), (0, 3)), ((0, 5), (0, 5))])) == [
        ("B12", "B14"),
        ("B16", "B16"),
    ]


def test_spreadsheet_reducer():
    from caerp.export.accounting_spreadsheet import NumericStringSpreadsheetReducer

    reducer = NumericStringSpreadsheetReducer()

    assert reducer.reduce(["2", "4", "*", "100.0", "/"]) == "2 * 4 / 100.0"
    assert (
        reducer.reduce(["2", "4", "*", "{fu}", "round", "/"]) == "2 * 4 / ROUND({fu})"
    )
    assert reducer.reduce(["100.0", "3", "/"]) == "100.0 / 3"
    assert reducer.reduce(["100.0", "2", "3", "+", "/"]) == "100.0 / (2 + 3)"
    assert (
        reducer.reduce(["{Total Marge}", "unary -", "10", "*", "100", "/"])
        == "-1 * {Total Marge} * 10 / 100"
    )
