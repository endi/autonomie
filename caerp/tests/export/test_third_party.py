from caerp.models.third_party import Customer
from sqla_inspect.csv import SqlaCsvExporter


def test_customer_export_number_format(mk_customer):
    # https://framagit.org/caerp/caerp/-/issues/3459
    customer = mk_customer(id=100000)
    exporter = SqlaCsvExporter(Customer)
    exporter.add_row(customer)
    assert exporter._datas[0]["Identifiant"] == 100000
