import datetime
import pytest

from caerp.compute.math_utils import floor_to_precision
from caerp.controllers.task.estimation import EstimationInvoicingController
from caerp.models.price_study import PriceStudyChapter


@pytest.fixture
def estimation_with_deposit(
    dbsession,
    estimation,
    mk_task_line,
    mk_discount_line,
    tva20,
    tva55,
    mk_product,
    product,
):
    estimation.deposit = 30

    estimation.line_groups[0].lines = [
        mk_task_line(cost=10000000, tva=2000, product=product),
        mk_task_line(cost=10000000, tva=2000, product=product),
        mk_task_line(cost=10000000, tva=550, product=mk_product(tva=tva55)),
    ]
    estimation.discounts = [
        mk_discount_line(amount=5000000, tva=2000),
    ]
    dbsession.merge(estimation)
    dbsession.flush()
    return estimation


# https://framagit.org/caerp/caerp/-/issues/3677
@pytest.fixture
def estimation_3677(
    get_csrf_request_with_db,
    dbsession,
    mk_estimation,
    mk_payment_line,
    mk_task_line,
    mk_task_line_group,
    tva10,
):
    empty_ht_estimation = mk_estimation(mode="ht")
    lines = [
        mk_task_line(cost=103313000, tva=tva10.value, mode="ht"),
        mk_task_line(cost=177008000, quantity=2.38, tva=tva10.value, mode="ht"),
        mk_task_line(cost=7963000, quantity=122.31, tva=tva10.value, mode="ht"),
    ]
    empty_ht_estimation.line_groups = [mk_task_line_group(lines=lines)]
    empty_ht_estimation.payment_lines = [
        mk_payment_line(amount=824201000),
        mk_payment_line(amount=0),
    ]
    empty_ht_estimation.deposit = 50
    empty_ht_estimation.manualDeliverables = 1
    dbsession.merge(empty_ht_estimation)
    dbsession.flush()
    empty_ht_estimation.update_payment_lines(get_csrf_request_with_db())
    return empty_ht_estimation


@pytest.fixture
def estimation_with_price_study(
    get_csrf_request_with_db,
    estimation,
    mk_price_study,
    mk_price_study_chapter,
    mk_price_study_product,
    mk_price_study_work,
    mk_price_study_work_item,
    tva,
    product,
):
    price_study = mk_price_study(task=estimation)
    chapter = mk_price_study_chapter(title="Chapitre 1", price_study=price_study)
    price_study_product = mk_price_study_product(
        supplier_ht=10000000,
        margin_rate=0.1,
        mode="supplier_ht",
        tva=tva,
        quantity=4,
        unity="m2",
        chapter=chapter,
    )
    price_study_work = mk_price_study_work(
        chapter=chapter,
        tva=tva,
        display_details=False,
        unity="m²",
    )
    for index in range(4):
        mk_price_study_work_item(
            price_study_work=price_study_work,
            mode="supplier_ht",
            supplier_ht=10000000,
            work_unit_quantity=index,
            quantity_inherited=True,
        )
    price_study.sync_amounts(sync_down=True)
    price_study.sync_with_task(
        get_csrf_request_with_db(),
    )
    return estimation


@pytest.fixture
def classic_invoice_estimation_price_study(
    dbsession,
    estimation_with_price_study,
    mk_invoice,
    mk_task_line_group,
    mk_task_line,
    tva,
    product,
):
    invoice1 = mk_invoice(estimation=estimation_with_price_study)
    invoice1.line_groups = [
        mk_task_line_group(
            lines=[mk_task_line(cost=1000000, tva=tva.value, product=product)]
        )
    ]
    dbsession.merge(invoice1)
    dbsession.flush()


@pytest.fixture
def mk_controller(get_csrf_request_with_db_and_user):
    def result(estimation):
        return EstimationInvoicingController(
            estimation, get_csrf_request_with_db_and_user()
        )

    return result


class TestEstimationInvoicingController:
    def test__get_common_invoice(
        self, mk_controller, estimation, user, mk_task_mention
    ):
        estimation.start_date = (datetime.date(2021, 2, 1),)
        estimation.first_visit = (datetime.date(2021, 1, 20),)
        estimation.end_date = ("Deux semaines après le début",)
        # #3809
        disabled_mention = mk_task_mention(active=False, label="Test", title="Titre")
        estimation.mentions.append(disabled_mention)
        controller = mk_controller(estimation)
        common_invoice = controller._get_common_invoice(
            user,
        )
        for key in (
            "company",
            "customer",
            "project",
            "business_type_id",
            "phase_id",
            "payment_conditions",
            "description",
            "address",
            "workplace",
            "start_date",
            "first_visit",
            "end_date",
            "insurance_id",
        ):
            assert getattr(common_invoice, key) == getattr(estimation, key)

        assert common_invoice.business_id is not None
        assert common_invoice.estimation == estimation
        assert common_invoice.status_user == user
        assert common_invoice.owner == user
        # Fix 3809
        for mention in estimation.mentions:
            if mention.active:
                assert mention in common_invoice.mentions
            else:
                assert mention not in common_invoice.mentions

    def test_get_common_invoice_ht_mode(self, mk_controller, estimation, user):
        controller = mk_controller(estimation)
        common_invoice = controller._get_common_invoice(
            user,
        )
        assert common_invoice.mode == estimation.mode
        assert common_invoice.mode == "ht"

    def test_get_common_invoice_ttc_mode(self, mk_controller, estimation, user):
        estimation.mode = "ttc"
        controller = mk_controller(estimation)
        common_invoice = controller._get_common_invoice(
            user,
        )
        assert common_invoice.mode == estimation.mode
        assert common_invoice.mode == "ttc"

    def test__get_ratio_by_tva(
        self, dbsession, mk_controller, estimation, mk_task_line
    ):
        estimation.line_groups[0].lines = [mk_task_line(cost=100000000)]
        dbsession.merge(estimation)
        dbsession.flush()
        controller = mk_controller(estimation)
        # TTC = 120000000
        ratio_by_tva = controller._get_ratio_by_tva(30000000)
        assert ratio_by_tva == {"default": 25}
        ratio_by_tva = controller._get_ratio_by_tva(31245000)
        assert ratio_by_tva == {"default": 26.0375}

    def test_deposit_invoice(self, mk_controller, estimation_with_deposit, user):
        controller = mk_controller(estimation_with_deposit)
        deposit_ttc = estimation_with_deposit.deposit_amount_ttc()
        deposit_invoice = controller.gen_deposit_invoice(
            user, "Facture d'acompte numéro 1", deposit_ttc
        )
        assert deposit_invoice.total() == deposit_ttc
        # Rappel du devis + acompte
        assert len(deposit_invoice.line_groups) == 2
        tvas = deposit_invoice.tva_ht_parts()

        assert tvas[550] == 10000000 * 30 / 100
        assert tvas[2000] == (2 * 10000000 - 5000000) * 30 / 100

    def test_deposit_invoice_discounts_multiple_products(
        self, mk_controller, estimation_with_deposit, user, mk_product, tva20
    ):
        # On change le compte produit de la Première ligne de tva 20%
        estimation_with_deposit.line_groups[0].lines[0].product = mk_product(tva=tva20)
        controller = mk_controller(estimation_with_deposit)
        deposit_ttc = estimation_with_deposit.deposit_amount_ttc()

        deposit_invoice = controller.gen_deposit_invoice(
            user, "Facture d'acompte numéro 1", deposit_ttc
        )
        assert deposit_invoice.total() == deposit_ttc
        # Rappel du devis + acompte
        assert len(deposit_invoice.line_groups) == 2
        tvas = deposit_invoice.tva_ht_parts()
        # On est dans le cas où on manque de déterminisme
        assert tvas[550] == 10000000 * 30 / 100
        assert tvas[2000] == (2 * 10000000 - 5000000) * 30 / 100

    def test_intermediate_invoice(
        self, dbsession, mk_controller, estimation, user, mk_task_line
    ):

        estimation.line_groups[0].lines = [mk_task_line(cost=100000000)]
        dbsession.merge(estimation)
        dbsession.flush()
        controller = mk_controller(estimation)
        invoice = controller.gen_intermediate_invoice(
            user, "Facture intermédiaire", 32150000, datetime.date(2018, 5, 1)
        )
        # Malgré la date demandée, on facture après le devis
        assert invoice.date == datetime.date.today()
        assert floor_to_precision(invoice.total()) == 32150000
        assert len(invoice.line_groups) == 2

    def test_intermediate_invoice_4267(
        self, dbsession, mk_controller, estimation, user, mk_task_line
    ):

        estimation.line_groups[0].lines = [
            mk_task_line(cost=100000000, product_id=None)
        ]
        dbsession.merge(estimation)
        dbsession.flush()
        controller = mk_controller(estimation)
        invoice = controller.gen_intermediate_invoice(
            user, "Facture intermédiaire", 32150000, datetime.date(2018, 5, 1)
        )
        # Malgré la date demandée, on facture après le devis
        assert invoice.date == datetime.date.today()
        assert floor_to_precision(invoice.total()) == 32150000
        assert len(invoice.line_groups) == 2

    def test__get_existing_invoices(self, mk_controller, estimation_with_deposit, user):
        controller: EstimationInvoicingController = mk_controller(
            estimation_with_deposit
        )

        invoice1 = controller.gen_deposit_invoice(user, "Acompte numéro 1", 10000000)
        assert controller._get_existing_invoices(invoice1) == []

        invoice2 = controller.gen_intermediate_invoice(
            user, "Facture intermédiaire", 32150000
        )
        assert controller._get_existing_invoices(invoice2) == [invoice1]

    def _test__collect_already_invoiced_groups(
        self, mk_controller, estimation_with_deposit, user
    ):
        controller: EstimationInvoicingController = mk_controller(
            estimation_with_deposit
        )

        invoice1 = controller.gen_deposit_invoice(user, "Acompte numéro 1", 10000000)
        assert controller._collect_already_invoiced_groups(invoice1) == []

        invoice2 = controller.gen_intermediate_invoice(
            user, "Facture intermédiaire", 32150000
        )
        assert controller._collect_already_invoiced_groups(invoice2) == [
            invoice1.line_groups[1:]
        ]

    # https://framagit.org/caerp/caerp/-/issues/4291
    def _test__collect_already_invoiced_chapters_mixed_bug4291(
        self,
        mk_controller,
        mk_invoice,
        estimation_with_price_study,
        classic_invoice_estimation_price_study,
        user,
    ):
        # On a un devis en edp et une facture classique raccrochée après
        controller: EstimationInvoicingController = mk_controller(
            estimation_with_price_study
        )
        invoice1 = mk_invoice(estimation=estimation_with_price_study)
        chapters = controller._collect_already_invoiced_chapters(invoice1)
        assert len(chapters) == 1

    # https://framagit.org/caerp/caerp/-/issues/4291
    def _test__collect_already_invoiced_groups_mixed_bug4291(
        self,
        dbsession,
        mk_controller,
        mk_invoice,
        mk_price_study,
        mk_price_study_chapter,
        mk_price_study_product,
        estimation_with_deposit,
        product,
        tva,
        user,
    ):
        # On a un devis classique et une facture avec edp
        # raccrochée après
        controller: EstimationInvoicingController = mk_controller(
            estimation_with_deposit
        )
        invoice1 = mk_invoice(estimation=estimation_with_deposit)
        study = mk_price_study(task=invoice1)
        chapter = mk_price_study_chapter(title="Chapitre ", price_study=study)
        chapter.products = [
            mk_price_study_product(
                ht=1000000, total_ht=5000000, product=product, tva=tva
            )
        ]

        dbsession.merge(invoice1)
        dbsession.flush()
        invoice2 = mk_invoice(estimation=estimation_with_deposit)
        chapters = controller._collect_already_invoiced_chapters(invoice2)
        assert isinstance(chapters[0], PriceStudyChapter)
        assert len(chapters) == 1

    def test_sold_invoice_direct(self, mk_controller, estimation_with_deposit, user):
        controller = mk_controller(estimation_with_deposit)
        date = datetime.date.today() + datetime.timedelta(days=5)
        invoice = controller.gen_sold_invoice(user, date)
        assert invoice.date == date
        assert invoice.financial_year == date.year
        assert invoice.total() == estimation_with_deposit.total()
        assert len(invoice.line_groups) == 1

    def test_sold_invoice_after_other_invoices(
        self, mk_controller, estimation_with_deposit, user
    ):
        controller: EstimationInvoicingController = mk_controller(
            estimation_with_deposit
        )
        invoice1 = controller.gen_deposit_invoice(user, "Acompte numéro 1", 10000000)

        invoice2 = controller.gen_intermediate_invoice(
            user, "Facture intermédiaire", 32150000
        )

        invoice3 = controller.gen_sold_invoice(user)
        groups = controller._collect_already_invoiced_groups(invoice3)
        # Acompte et intermédiaire
        assert len(groups) == 2

        assert (
            invoice1.total() + invoice2.total() + invoice3.total()
            == estimation_with_deposit.total()
        )

    @pytest.mark.xfail
    def test_manual_payment_line_amounts_bug_3677(
        self, mk_controller, estimation_3677, user
    ):
        controller = mk_controller(estimation_3677)
        amount = estimation_3677.deposit_amount_ttc()
        deposit_invoice = controller.gen_deposit_invoice(user, "acompte", amount)
        intermediate_invoice = controller.gen_intermediate_invoice(
            user, "Facture intermédiaire", 824201000
        )
        sold_invoice = controller.gen_sold_invoice(user)
        assert (
            deposit_invoice.total()
            + intermediate_invoice.total()
            + sold_invoice.total()
            == estimation_3677.total()
        )

    def test_direct_sold_from_price_study(
        self, mk_controller, estimation_with_price_study, user
    ):
        controller = mk_controller(estimation_with_price_study)
        date = datetime.date.today() + datetime.timedelta(days=5)
        invoice = controller.gen_sold_invoice(user, date)
        assert invoice.date == date
        assert invoice.financial_year == date.year
        assert invoice.total() == estimation_with_price_study.total()

    def test_sold_from_price_study_after_other_invoices(
        self, mk_controller, estimation_with_price_study, user
    ):
        controller = mk_controller(estimation_with_price_study)
        invoice1 = controller.gen_deposit_invoice(user, "Acompte numéro 1", 1000000)
        invoice2 = controller.gen_intermediate_invoice(
            user, "Facture intermédiaire", 32150000
        )
        invoice = controller.gen_sold_invoice(user)
        assert (
            invoice.total() + invoice1.total() + invoice2.total()
            == estimation_with_price_study.total()
        )

    # https://framagit.org/caerp/caerp/-/issues/4291
    def test_mixed_edp_classic_bug4291_cas1(
        self,
        dbsession,
        mk_controller,
        estimation_with_price_study,
        mk_invoice,
        mk_task_line_group,
        mk_task_line,
        user,
        tva,
        product,
    ):
        controller = mk_controller(estimation_with_price_study)
        invoice1 = mk_invoice(
            estimation=estimation_with_price_study, date=datetime.date(2024, 7, 1)
        )
        invoice1.line_groups = [
            mk_task_line_group(
                lines=[mk_task_line(cost=1000000, tva=tva.value, product=product)]
            )
        ]
        dbsession.merge(invoice1)
        dbsession.flush()
        invoice2 = controller.gen_sold_invoice(user, datetime.date(2024, 7, 1))
        assert (
            invoice1.total() + invoice2.total() == estimation_with_price_study.total()
        )

    # https://framagit.org/caerp/caerp/-/issues/4291
    def test_mixed_edp_classic_bug4291_cas2(
        self,
        get_csrf_request_with_db,
        dbsession,
        mk_controller,
        estimation_with_deposit,
        mk_invoice,
        mk_price_study,
        mk_price_study_chapter,
        mk_price_study_product,
        user,
        tva,
        product,
    ):
        request = get_csrf_request_with_db()
        controller = mk_controller(estimation_with_deposit)
        invoice1 = mk_invoice(
            estimation=estimation_with_deposit, date=datetime.date(2024, 7, 1)
        )
        price_study = mk_price_study(task=invoice1)
        price_study_chapter = mk_price_study_chapter(
            price_study=price_study, title="Chapitre 1"
        )
        price_study_chapter.products = [
            mk_price_study_product(
                ht=1000000, quantity=5, total_ht=5000000, product=product, tva=tva
            )
        ]
        price_study.sync_amounts(sync_down=True)
        price_study.sync_with_task(request)
        dbsession.merge(invoice1)
        dbsession.flush()

        invoice2 = controller.gen_sold_invoice(user, datetime.date(2024, 7, 1))

        assert invoice1.total() + invoice2.total() == estimation_with_deposit.total()
