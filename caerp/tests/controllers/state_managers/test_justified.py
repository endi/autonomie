from caerp.controllers.state_managers import set_justified_status


def test_expense_justified_status(csrf_request_with_db_and_user, mk_expense_sheet):
    sheet = mk_expense_sheet()
    set_justified_status(csrf_request_with_db_and_user, sheet, True, comment="Comment")

    assert sheet.justified is True
    assert sheet.statuses[0].comment == "Comment"
    assert sheet.statuses[0].user_id == csrf_request_with_db_and_user.identity.id
