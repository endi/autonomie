from caerp.controllers.state_managers import set_validation_status
from caerp.models.status import StatusLogEntry


def test_supplier_invoice_validation_sets_official_number(
    mk_supplier_invoice,
    csrf_request_with_db_and_user,
    content,
):
    supplier_invoice_1 = mk_supplier_invoice()
    supplier_invoice_2 = mk_supplier_invoice()

    assert supplier_invoice_1.official_number is None
    assert supplier_invoice_2.official_number is None

    set_validation_status(
        csrf_request_with_db_and_user,
        supplier_invoice_1,
        "valid",
        comment="Comment",
    )
    assert supplier_invoice_1.official_number == "1"

    set_validation_status(csrf_request_with_db_and_user, supplier_invoice_2, "valid")
    assert supplier_invoice_2.official_number == "2"


def test_supplier_invoice_validation_status_log(
    mk_supplier_invoice,
    get_csrf_request_with_db,
    mk_user,
):
    user1 = mk_user()
    user2 = mk_user()
    supplier_invoice_1 = mk_supplier_invoice()
    request = get_csrf_request_with_db(user=user1)
    set_validation_status(
        request,
        supplier_invoice_1,
        "wait",
        comment="En attente : 😀 😁	😂	😃	😄	😅",
    )
    request = get_csrf_request_with_db(user=user2)
    set_validation_status(
        request,
        supplier_invoice_1,
        "valid",
        comment="Comment",
    )

    entries = (
        StatusLogEntry.query()
        .filter(StatusLogEntry.node_id == supplier_invoice_1.id)
        .all()
    )
    assert len(entries) == 2
    assert entries[0].status == "wait"
    assert entries[1].status == "valid"
    assert entries[0].comment == "En attente : 😀 😁	😂	😃	😄	😅"
    assert entries[1].comment == "Comment"
    assert entries[0].user_id == user1.id
    assert entries[1].user_id == user2.id


def test_externalsupplier_invoice_validation_doesnt_set_resulted(
    mk_supplier_invoice,
    mk_supplier_invoice_line,
    pyramid_request,
    csrf_request_with_db_and_user,
    content,
):
    # negative supplier_invoice but external
    supplier_invoice = mk_supplier_invoice()
    mk_supplier_invoice_line(supplier_invoice=supplier_invoice, ht=-50000, tva=-10000)

    set_validation_status(
        csrf_request_with_db_and_user,
        supplier_invoice,
        "valid",
    )
    assert supplier_invoice.worker_paid_status == "waiting"
    assert supplier_invoice.paid_status == "waiting"
    assert supplier_invoice.supplier_paid_status == "waiting"


def test_internalsupplier_invoice_validation_sets_resulted_when_negative(
    mk_internalsupplier_invoice,
    mk_supplier_invoice_line,
    csrf_request_with_db_and_user,
):
    # negative internalsupplier_invoice
    supplier_invoice = mk_internalsupplier_invoice()
    mk_supplier_invoice_line(supplier_invoice=supplier_invoice, ht=-50000, tva=-10000)

    set_validation_status(
        csrf_request_with_db_and_user,
        supplier_invoice,
        "valid",
    )

    assert supplier_invoice.worker_paid_status == "resulted"
    assert supplier_invoice.paid_status == "resulted"
    assert supplier_invoice.supplier_paid_status == "resulted"


def test_internalsupplier_invoice_validation_sets_not_resulted_when_positive(
    mk_internalsupplier_invoice,
    mk_supplier_invoice_line,
    csrf_request_with_db_and_user,
):
    # positive internalsupplier_invoice
    supplier_invoice = mk_internalsupplier_invoice()
    mk_supplier_invoice_line(supplier_invoice=supplier_invoice, ht=50000, tva=10000)

    set_validation_status(
        csrf_request_with_db_and_user,
        supplier_invoice,
        "valid",
    )

    assert supplier_invoice.worker_paid_status == "waiting"
    assert supplier_invoice.paid_status == "waiting"
    assert supplier_invoice.supplier_paid_status == "waiting"


def test_expensesheet_validation(mk_expense_sheet, csrf_request_with_db_and_user):
    sheet = mk_expense_sheet()
    set_validation_status(
        csrf_request_with_db_and_user, sheet, "valid", comment="Comment"
    )
    assert sheet.status == "valid"

    entries = StatusLogEntry.query().filter(StatusLogEntry.node_id == sheet.id).all()
    assert len(entries) == 1
    assert entries[0].status == "valid"
    assert entries[0].comment == "Comment"
    assert entries[0].user_id == csrf_request_with_db_and_user.identity.id
    assert entries[0].node_id == sheet.id


def test_invoice_validation(mk_invoice, csrf_request_with_db_and_user):
    invoice = mk_invoice()
    set_validation_status(
        csrf_request_with_db_and_user, invoice, "valid", comment="Comment"
    )
    assert invoice.status == "valid"
    assert invoice.official_number == "1"

    entries = StatusLogEntry.query().filter(StatusLogEntry.node_id == invoice.id).all()
    assert len(entries) == 1
    assert entries[0].status == "valid"
    assert entries[0].comment == "Comment"
    assert entries[0].user_id == csrf_request_with_db_and_user.identity.id
    assert entries[0].node_id == invoice.id


def test_estimation_validation(mk_estimation, csrf_request_with_db_and_user):
    estimation = mk_estimation()
    set_validation_status(
        csrf_request_with_db_and_user, estimation, "valid", comment="Comment"
    )
    assert estimation.status == "valid"

    entries = (
        StatusLogEntry.query().filter(StatusLogEntry.node_id == estimation.id).all()
    )
    assert len(entries) == 1
    assert entries[0].status == "valid"
    assert entries[0].comment == "Comment"
    assert entries[0].user_id == csrf_request_with_db_and_user.identity.id
    assert entries[0].node_id == estimation.id
