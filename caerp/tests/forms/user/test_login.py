import pytest
import colander
from pyramid import testing


def test_password_change_schema(user, user2, mk_login, config, pyramid_request):
    from caerp.forms.user.login import get_password_schema

    config.set_security_policy(testing.DummySecurityPolicy(identity=user))
    schema = get_password_schema(pyramid_request)
    pyramid_request.context = mk_login(login="second login", user=user2)
    schema = schema.bind(request=pyramid_request)

    result = schema.deserialize({"pwd_hash": "New pass"})

    assert result["pwd_hash"] == "New pass"


def test_mypassword_change_schema(login, user, config, pyramid_request):
    from caerp.forms.user.login import get_password_schema

    config.set_security_policy(testing.DummySecurityPolicy(identity=user))
    schema = get_password_schema(pyramid_request)
    pyramid_request.context = login

    schema = schema.bind(request=pyramid_request)

    result = schema.deserialize({"password": "pwd", "pwd_hash": "New pass"})

    assert result["pwd_hash"] == "New pass"

    with pytest.raises(colander.Invalid):
        schema.deserialize({"password": "ooo", "pwd_hash": "New pass"})


def test_add_schema(dbsession, pyramid_request, login, groups):

    from caerp.forms.user.login import get_add_edit_schema

    pyramid_request.dbsession = dbsession
    schema = get_add_edit_schema(pyramid_request, "entrepreneur")
    schema = schema.bind(request=pyramid_request)
    result = schema.deserialize(
        {
            "login": "test2",
            "pwd_hash": "oo",
            "groups": ["trainer"],
            "user_id": 1500,
        }
    )

    assert "pwd_hash" in result

    # pwd_hash required
    with pytest.raises(colander.Invalid):
        schema.deserialize(
            {
                "login": "test2",
                "pwd_hash": "",
                "account_type": "entrepreneur",
                "groups": ["trainer"],
                "user_id": 1500,
            }
        )

    # "login" already used
    with pytest.raises(colander.Invalid):
        schema.deserialize(
            {
                "login": "login",
                "pwd_hash": "ooo",
                "account_type": "entrepreneur",
                "groups": ["trainer"],
                "user_id": 1500,
            }
        )

    with pytest.raises(colander.Invalid):
        schema.deserialize(
            {
                "login": "test2",
                "pwd_hash": "ooo",
                "account_type": "falseon",
                "groups": ["trainer"],
                "user_id": 1500,
            }
        )


def test_edit_schema_login_context(dbsession, pyramid_request, login, user, groups):

    from caerp.forms.user.login import get_add_edit_schema
    from caerp.models.user.login import Login
    from caerp.models.user.user import User

    user2 = User(email="a@a.fr", lastname="lastname2", firstname="firstname2")
    dbsession.add(user2)
    dbsession.flush()

    item = Login(user_id=user2.id, login="test2")
    item.set_password("pwd2")
    dbsession.add(item)
    dbsession.flush()

    pyramid_request.dbsession = dbsession
    pyramid_request.context = item

    schema = get_add_edit_schema(pyramid_request, item.account_type, edit=True)
    schema = schema.bind(request=pyramid_request)
    result = schema.deserialize(
        {
            "login": "test2",
            "pwd_hash": "",
            "groups": ["trainer"],
            "user_id": user2.id,
        }
    )

    assert "pwd_hash" not in result

    result = schema.deserialize(
        {
            "login": "test2",
            "pwd_hash": "notpwd2",
            "groups": ["trainer"],
            "user_id": user2.id,
        }
    )

    assert "pwd_hash" in result

    # Login already used
    with pytest.raises(colander.Invalid):
        schema.deserialize(
            {
                "login": "login",
                "pwd_hash": "",
                "groups": ["trainer"],
                "user_id": user2.id,
            }
        )

    # User already linked to Login class
    with pytest.raises(colander.Invalid):
        schema.deserialize(
            {
                "login": "test2",
                "pwd_hash": "ooo",
                "groups": ["trainer"],
                "user_id": user.id,
            }
        )

    # wrong group
    with pytest.raises(colander.Invalid):
        schema.deserialize(
            {
                "login": "test2",
                "pwd_hash": "ooo",
                "user_id": user2.id,
                "groups": ["falseone"],
            }
        )


def test_edit_schema_user_context(dbsession, pyramid_request, login, user, groups):

    from caerp.forms.user.login import get_add_edit_schema
    from caerp.models.user.login import Login
    from caerp.models.user.user import User

    user2 = User(email="a@a.fr", lastname="lastname2", firstname="firstname2")
    dbsession.add(user2)
    dbsession.flush()

    item = Login(user_id=user2.id, login="test2")
    item.set_password("pwd2")
    dbsession.add(item)
    dbsession.flush()

    pyramid_request.dbsession = dbsession
    pyramid_request.context = user2

    schema = get_add_edit_schema(pyramid_request, item.account_type, edit=True)
    schema = schema.bind(request=pyramid_request)
    result = schema.deserialize(
        {
            "login": "test2",
            "pwd_hash": "",
            "groups": ["trainer"],
            "user_id": user2.id,
        }
    )

    assert "pwd_hash" not in result

    result = schema.deserialize(
        {
            "login": "test2",
            "pwd_hash": "notpwd2",
            "groups": ["trainer"],
            "user_id": user2.id,
        }
    )

    assert "pwd_hash" in result

    # Login already used
    with pytest.raises(colander.Invalid):
        schema.deserialize(
            {
                "login": login.login,
                "pwd_hash": "",
                "user_id": user2.id,
                "groups": ["trainer"],
            }
        )

    # User already linked to Login class
    with pytest.raises(colander.Invalid):
        schema.deserialize(
            {
                "login": "test2",
                "pwd_hash": "",
                "user_id": user.id,
            }
        )

    # wrong group
    with pytest.raises(colander.Invalid):
        schema.deserialize(
            {
                "login": "test2",
                "pwd_hash": "",
                "account_type": "entrepreneur",
                "groups": ["unknown group"],
                "user_id": user2.id,
            }
        )


def test_auth_schema(dbsession, login):

    from caerp.forms.user.login import get_auth_schema

    schema = get_auth_schema()
    result = schema.deserialize({"login": login.login, "password": "pwd"})
    assert "password" in result

    with pytest.raises(colander.Invalid):
        schema.deserialize({"login": "nottest", "password": "pwd"})

    with pytest.raises(colander.Invalid):
        schema.deserialize({"login": "login", "password": "notpwd"})
