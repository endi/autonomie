from datetime import date

import pytest

from caerp.dataqueries.queries.ob_porteurs_actifs_periode import (
    doctypes_to_flags,
    OBActiveOrSupportedESQuery,
)
from caerp.models.user.userdatas import (
    SocialDocTypeOption,
    UserDatasSocialDocTypes,
)


@pytest.fixture
def ob_social_doc_types(dbsession):
    dts = [
        SocialDocTypeOption(label=name)
        for name in [
            "Avis situation Pôle Emploi",
            "Attestation CAF(si RSA)",
            "RIB",
            "Attestation assurance véhicule",
            "Attestation propriété ou facture vélo (si kms vélo)",
            "Copie carte grise",
            "Copie permis de conduire",
            "Copie carte d'identité",
            "Attestation carte vitale",
            "Copie des diplômes relatifs au projet",
            "Chèque de caution accident du travail",
            "Attestation RQTH",
            "Questionnaire FSE",
            "Déclaration Minimis",
        ]
    ]
    dbsession.add_all(dts)
    dbsession.flush()
    return dts


def test_doctypes_to_flags(userdatas, ob_social_doc_types, dbsession):
    a1 = UserDatasSocialDocTypes(
        userdatas=userdatas, status=True, doctype=ob_social_doc_types[0]
    )
    a2 = UserDatasSocialDocTypes(
        userdatas=userdatas, status=False, doctype=ob_social_doc_types[-1]
    )
    dbsession.add_all([a1, a2])
    dbsession.flush()

    assert doctypes_to_flags(
        userdatas,
        [
            "Avis situation Pôle Emploi",
            "Déclaration Minimis",
            "Attestation RQTH",
        ],
    ) == [
        True,
        False,
        False,
    ]


def test_OBActiveOrSupportedESQuery(request_with_config_and_db, userdatas):
    dataq = OBActiveOrSupportedESQuery(request=request_with_config_and_db, context=None)
    dataq.set_dates(date(2023, 1, 1), date(2023, 12, 31))

    nb_headers_cols = len(dataq.headers())  # just checks it does not crash

    row = dataq.data_row_from_userdatas(userdatas)
    assert len(row) + 1 == nb_headers_cols
    assert row[1:] == [
        "",
        "",
        "Userdatas",
        None,
        "userdatas",
        "userdatas@test.fr",
        None,
        None,
        None,
        None,
        None,
        "",
        "",
        "",
        "NON",
        "",
        "OUI",
        "OUI",
        "OUI",
        "OUI",
        "",
        "",
        "",
        None,
        "",
        None,
        None,
        "",
        "NON",
        "None",
        "",
        0,
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        151.0,
        1811,
        "NON",
        "",
        "",
        "",
        "NON",
        "NON",
        "NON",
        "NON",
        "",
        "",
        "NON",
    ]
