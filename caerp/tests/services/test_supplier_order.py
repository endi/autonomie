import datetime
import pytest
from freezegun import freeze_time

from caerp.models.supply.supplier_order import SupplierOrder
from caerp.services.supplier_order import get_supplier_orders_years


@pytest.fixture
def order_2017(mk_supplier_order):
    mk_supplier_order(created_at=datetime.date(2017, 10, 10))


@pytest.fixture
def order_2017bis(mk_supplier_order):
    mk_supplier_order(created_at=datetime.date(2017, 1, 1))


@pytest.fixture
def order_2018(mk_supplier_order):
    mk_supplier_order(created_at=datetime.date(2018, 1, 1))


@freeze_time("2019")
def test_get_supplier_orders_years_no_order(request_with_config_and_db):
    assert get_supplier_orders_years({"request": request_with_config_and_db}) == [2019]


@freeze_time("2019")
def test_get_supplier_orders_years_one_order(request_with_config_and_db, order_2017):
    assert get_supplier_orders_years({"request": request_with_config_and_db}) == [
        2017,
        2019,
    ]


@freeze_time("2019")
def test_get_supplier_orders_years_two_orders_same_year(
    request_with_config_and_db, order_2017, order_2017bis
):
    assert get_supplier_orders_years({"request": request_with_config_and_db}) == [
        2017,
        2019,
    ]


@freeze_time("2019")
def test_get_supplier_orders_years_three_orders_different_year(
    request_with_config_and_db,
    order_2017,
    order_2017bis,
    order_2018,
):
    assert get_supplier_orders_years({"request": request_with_config_and_db}) == [
        2017,
        2018,
        2019,
    ]
