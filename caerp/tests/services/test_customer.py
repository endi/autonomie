import datetime
from caerp.models.config import Config
from caerp.services.rgpd.customer import (
    get_customers_not_used_for,
    check_customer_expired,
)
from caerp.utils.datetimes import date_to_datetime


def test_get_customers_not_used_for(request_with_config_and_db, mk_customer, mk_task):
    today = datetime.date.today()
    delay = 365
    # older than delay threshold
    older_date = today - datetime.timedelta(days=delay + 1)
    newer_date = today - datetime.timedelta(days=delay - 1)
    customer = mk_customer(type="individual", created_at=date_to_datetime(older_date))
    mk_task(customer=customer, date=older_date)
    assert customer in get_customers_not_used_for(
        request_with_config_and_db, days_threshold=delay
    )
    mk_task(customer=customer, date=newer_date)
    assert customer not in get_customers_not_used_for(request_with_config_and_db, delay)

    customer2 = mk_customer(type="company", created_at=date_to_datetime(older_date))
    assert customer2 not in get_customers_not_used_for(
        request_with_config_and_db, delay
    )

    customer3 = mk_customer(type="individual", created_at=date_to_datetime(newer_date))
    assert customer3 not in get_customers_not_used_for(
        request_with_config_and_db, delay
    )


def test_customer_expired(request_with_config_and_db, mk_customer, mk_task):
    today = datetime.date.today()
    delay = 800
    older_date = today - datetime.timedelta(days=delay + 1)
    newer_date = today - datetime.timedelta(days=delay - 1)
    customer = mk_customer(type="individual", created_at=date_to_datetime(older_date))
    assert check_customer_expired(request_with_config_and_db, customer.id, delay)
    mk_task(customer=customer, date=today)
    assert not check_customer_expired(request_with_config_and_db, customer.id, delay)
    customer = mk_customer(type="individual", created_at=date_to_datetime(newer_date))
    assert not check_customer_expired(request_with_config_and_db, customer.id, delay)

    Config.set("rgpd_customers_retention_days", delay - 5)
    assert check_customer_expired(request_with_config_and_db, customer.id)  # not found
