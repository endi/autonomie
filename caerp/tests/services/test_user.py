import datetime
from caerp.services.rgpd.user import (
    get_userdatas_not_used_for,
    get_accounts_not_used_for,
)


def test_get_userdatas_not_used_for(
    request_with_config_and_db, mk_user, mk_userdatas, mk_login, mk_user_connection
):
    now = datetime.datetime.now()
    delay = 60
    older_date = now - datetime.timedelta(days=delay + 1)
    newer_date = now - datetime.timedelta(days=delay - 1)

    user1 = mk_user(email="user1")
    userdatas1 = mk_userdatas(user=user1, created_at=older_date)
    mk_login(user=user1, login="user1")

    mk_user_connection(user=user1, month_last_connection=older_date)
    assert userdatas1 in get_userdatas_not_used_for(request_with_config_and_db, delay)

    # created_at de la fiche de GS est trop récente
    user2 = mk_user(email="user2")
    userdatas2 = mk_userdatas(user=user2, created_at=newer_date)
    mk_login(user=user2, login="user2")

    # mk_user_connection(user=user2, month_last_connection=older_date)
    assert userdatas2 not in get_userdatas_not_used_for(
        request_with_config_and_db, delay
    )

    # S'est connecté plus récemment
    user3 = mk_user(email="user3")
    userdatas3 = mk_userdatas(user=user3, created_at=older_date)
    mk_login(user=user3, login="user3")
    # user1 is not used
    mk_user_connection(user=user3, month_last_connection=newer_date)
    assert userdatas3 not in get_userdatas_not_used_for(
        request_with_config_and_db, delay
    )


def test_get_accounts_not_used_for(
    request_with_config_and_db, mk_user, mk_login, mk_user_connection
):
    now = datetime.datetime.now()
    delay = 60
    older_date = now - datetime.timedelta(days=delay + 1)
    newer_date = now - datetime.timedelta(days=delay - 31)

    user = mk_user(email="user1")
    mk_login(user=user, login="user1")

    mk_user_connection(user=user, month_last_connection=older_date)
    assert user in get_accounts_not_used_for(request_with_config_and_db, delay)

    mk_user_connection(user=user, month_last_connection=newer_date)
    assert user not in get_accounts_not_used_for(request_with_config_and_db, delay)
