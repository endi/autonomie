import datetime
from caerp.tests.tools import Dummy


def test__find_paid_amount():
    from caerp.utils.notification.task import _find_paid_amount

    multi_tva_task = Dummy(
        get_tvas=lambda: {1960: 2, 2000: 2, 300: 3},
        payments=[
            Dummy(
                date=datetime.datetime(2020, 1, 1),
                amount=100000000,
                tva=1960,
            ),
            Dummy(
                date=datetime.datetime(2024, 1, 1),
                amount=100000000,
                tva=2000,
            ),
            Dummy(
                date=datetime.datetime(2024, 1, 1),
                amount=100000000,
                tva=1960,
            ),
        ],
    )
    assert _find_paid_amount(multi_tva_task) == 200000000
    multi_tva_task.payments[-1].tva = 2000
    assert _find_paid_amount(multi_tva_task) == 100000000

    mono_tva_task = Dummy(
        get_tvas=lambda: {1960: 2},
        payments=[
            Dummy(
                date=datetime.datetime(2020, 1, 1),
                amount=100000000,
                tva=2000,
            ),
            Dummy(
                date=datetime.datetime(2024, 1, 1),
                amount=100000000,
                tva=2000,
            ),
            Dummy(
                date=datetime.datetime(2024, 1, 1),
                amount=100000000,
                tva=2000,
            ),
        ],
    )
    assert _find_paid_amount(mono_tva_task) == 100000000


def test__get_body(
    dbsession,
    get_csrf_request_with_db,
    mk_invoice,
    mk_task_line,
    tva10,
    tva20,
    mk_payment,
):
    request = get_csrf_request_with_db()
    from caerp.utils.notification.task import _get_body

    invoice = mk_invoice()
    invoice.line_groups[0].lines = [
        mk_task_line(cost=100000000, tva=2000),
        mk_task_line(cost=200000000, tva=1000),
    ]
    invoice.payments = [
        mk_payment(task_id=invoice.id, amount=100000000, tva=tva20),
        mk_payment(task_id=invoice.id, amount=33270000, tva=tva10),
    ]
    dbsession.merge(invoice)
    dbsession.flush()

    body_message = _get_body(request, invoice, "paid")

    assert "1332,70 € a été encaissé." in body_message
    assert "2067,30" in body_message
