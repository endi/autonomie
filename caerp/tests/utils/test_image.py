"""
    Test image resizing
"""
import pytest
import os
from caerp.tests.conftest import DATASDIR

from PIL import Image

from caerp.utils.image import (
    ImageResizer,
    ImageRatio,
)


@pytest.fixture
def ratio():
    return ImageRatio(5, 1)


@pytest.fixture
def resizer():
    return ImageResizer(800, 200)


def test_ratio_not_affect_equal(ratio):
    with open(os.path.join(DATASDIR, "entete5_1.png"), "rb") as image:
        filedict = {"fp": image, "filename": "entete5_1.png", "mimetype": "image/png"}
        image2 = ratio(filedict)["fp"]
        assert Image.open(image2).size == Image.open(image).size


def test_ratio_not_affect_less(ratio):
    with open(os.path.join(DATASDIR, "entete10_1.png"), "rb") as image:
        filedict = {"fp": image, "filename": "entete10_1.png", "mimetype": "image/png"}
        image2 = ratio(filedict)["fp"]
        assert Image.open(image2).size == Image.open(image).size


def test_ratio(ratio):
    with open(os.path.join(DATASDIR, "entete2_1.png"), "rb") as image:
        filedict = {"fp": image, "filename": "entete2_1.png", "mimetype": "image/png"}
        image2 = ratio(filedict)["fp"]

    img_obj2 = Image.open(image2)
    width, height = img_obj2.size
    assert width / height == 5


def test_resize_width(resizer):
    with open(os.path.join(DATASDIR, "entete2_1.png"), "rb") as image:
        filedict = {"fp": image, "filename": "entete2_1.png", "mimetype": "image/png"}
        image2 = resizer(filedict)["fp"]

    assert Image.open(image2).size[0] == 400
    assert Image.open(image2).size[1] == 200


def test_resize_height(resizer):
    with open(os.path.join(DATASDIR, "entete5_1.png"), "rb") as image:
        filedict = {"fp": image, "filename": "entete5_1.png", "mimetype": "image/png"}
        image2 = resizer(filedict)["fp"]

    assert Image.open(image2).size[0] == 800
    assert Image.open(image2).size[1] == 160


def test_resize_cmyk_bug880(resizer):
    with open(os.path.join(DATASDIR, "cmyk.jpg"), "rb") as image:
        filedict = {"fp": image, "filename": "cmyk.jpg", "mimetype": "image/png"}
        result = resizer(filedict)

    assert Image.open(result["fp"]).size[0] <= 200
    assert Image.open(result["fp"]).mode == "RGB"
    assert result["filename"] == "cmyk.png"
    assert result["mimetype"] == "image/png"


def test_dontresize_bug4549():
    """
    https://framagit.org/caerp/caerp/-/issues/4549
    """
    resizer = ImageResizer(250, 250)
    with open(os.path.join(DATASDIR, "image_200_200.png"), "rb") as image:
        filedict = {"fp": image, "filename": "cmyk.jpg", "mimetype": "image/png"}
        result = resizer(filedict)

    # Don't affect since with and height < 250
    assert Image.open(result["fp"]).size[0] == 200
    resizer = ImageResizer(150, 250)
    with open(os.path.join(DATASDIR, "image_200_200.png"), "rb") as image:
        filedict = {"fp": image, "filename": "cmyk.jpg", "mimetype": "image/png"}
        result = resizer(filedict)

    # width > 150
    assert Image.open(result["fp"]).width == 150
    resizer = ImageResizer(250, 150)
    with open(os.path.join(DATASDIR, "image_200_200.png"), "rb") as image:
        filedict = {"fp": image, "filename": "cmyk.jpg", "mimetype": "image/png"}
        result = resizer(filedict)

    #  height > 150
    assert Image.open(result["fp"]).width == 150

    resizer = ImageResizer(250, 250, "PNG", 1900)
    with open(os.path.join(DATASDIR, "image_200_200.png"), "rb") as image:
        filedict = {"fp": image, "filename": "file.png", "mimetype": "image/png"}
        result = resizer(filedict)
        # no resizing should have been done since the file is smaller
        # than the size limit
        assert result["fp"] == image
