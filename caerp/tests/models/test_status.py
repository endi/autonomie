def test_validation_status_holder_service(
    supplier_order, supplier_invoice, invoiced_supplier_order, dbsession
):
    from caerp.models.supply import SupplierOrder, SupplierInvoice
    from caerp.models.status import ValidationStatusHolderService

    service = ValidationStatusHolderService()
    query = service.waiting(SupplierOrder, SupplierInvoice)
    assert query.count() == 0

    supplier_order.status = "wait"
    dbsession.merge(supplier_order)
    dbsession.flush()

    query = service.waiting(SupplierOrder, SupplierInvoice)
    assert query.count() == 1

    supplier_invoice.status = "wait"
    dbsession.merge(supplier_invoice)
    dbsession.flush()

    query = service.waiting(SupplierOrder, SupplierInvoice)
    assert query.count() == 2
    assert supplier_order in query
    assert supplier_invoice in query
