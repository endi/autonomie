<h1 class='${css}'>
    ${title|n}
    % if status_label:
    <small>(${status_label})</small>
    % endif
</h1>
